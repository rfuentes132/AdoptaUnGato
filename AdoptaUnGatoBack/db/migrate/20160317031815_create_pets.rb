class CreatePets < ActiveRecord::Migration
  def change
    create_table :pets do |t|
      t.string :pet_name
      t.string :picture
      t.boolean :sex
      t.string :ubication, null:false
      t.string :video
      t.string :color
      t.string :age, null:false
      t.string :description
      t.references :person, index: true, foreign_key: true
      t.references :pet_kind, index: true, foreign_key: true
      t.boolean :adopted
      t.timestamps null: false
    end
  end
end
