class CreatePetKinds < ActiveRecord::Migration
  def change
    create_table :pet_kinds do |t|
      t.string :species

      t.timestamps null: false
    end
  end
end
