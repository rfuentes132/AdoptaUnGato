class CreateReputations < ActiveRecord::Migration
  def change
    create_table :reputations do |t|
      t.integer :vote
      t.string :review
      t.references :person, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
