class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :username, null: false
      t.string :email, null: false
      t.string :password_digest, null:false
      t.string :token
      t.string :phone_number, null:false
      t.boolean :admin
      t.integer :ranking

      t.timestamps null: false
    end
  end
end
