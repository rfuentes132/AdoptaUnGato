# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160317031815) do

  create_table "people", force: :cascade do |t|
    t.string   "username",        limit: 255, null: false
    t.string   "email",           limit: 255, null: false
    t.string   "password_digest", limit: 255, null: false
    t.string   "token",           limit: 255
    t.string   "phone_number",    limit: 255, null: false
    t.boolean  "admin"
    t.integer  "ranking",         limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "pet_kinds", force: :cascade do |t|
    t.string   "species",    limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "pets", force: :cascade do |t|
    t.string   "pet_name",    limit: 255
    t.string   "picture",     limit: 255
    t.boolean  "sex"
    t.string   "ubication",   limit: 255, null: false
    t.string   "video",       limit: 255
    t.string   "color",       limit: 255
    t.string   "age",         limit: 255, null: false
    t.string   "description", limit: 255
    t.integer  "person_id",   limit: 4
    t.integer  "pet_kind_id", limit: 4
    t.boolean  "adopted"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "pets", ["person_id"], name: "index_pets_on_person_id", using: :btree
  add_index "pets", ["pet_kind_id"], name: "index_pets_on_pet_kind_id", using: :btree

  create_table "reputations", force: :cascade do |t|
    t.integer  "vote",       limit: 4
    t.string   "review",     limit: 255
    t.integer  "person_id",  limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "reputations", ["person_id"], name: "index_reputations_on_person_id", using: :btree

  add_foreign_key "pets", "people"
  add_foreign_key "pets", "pet_kinds"
  add_foreign_key "reputations", "people"
end
