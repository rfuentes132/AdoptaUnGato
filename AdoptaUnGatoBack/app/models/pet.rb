class Pet < ActiveRecord::Base
  belongs_to :person
  belongs_to :pet_kind

  validates :ubication, presence: true
  validates :age, presence: true
  before_create :set_adopted

  def set_adopted
  	self.adopted = true
  end
end
