class PetKind < ActiveRecord::Base
	has_many :pets

	def delete_related_pets
		self.pets.each { |i|
		i.destroy}
	end
end