class Person < ActiveRecord::Base
	has_many :pets
	has_many :reputations
	has_secure_password

	validates :phone_number, length: { is: 11 }

  validates :username, presence: true, uniqueness: true

  validates :email, presence: true, uniqueness: true
  formato = /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/
  validates :email, format: formato

  validates_presence_of :password, unless: :password_blank?
  validates_length_of :password, maximum: 20, minimum: 6,  unless: :password_blank?

	before_save :calculate_ranking
	
	def password_blank?
		self.password.nil?
	end

	def delete_related_pets
		self.pets.each { |i|
		i.destroy}
	end
	

	private


  def calculate_ranking #tiene que hacer update para que se cambie el campo.
		total = 0
  	self.reputations.each do |s|
	  	total += s.vote
	  	self.ranking = total
  	end
	end
end
