class PeopleController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, :with => :r_not_found
  before_action :authenticate, except:[:create, :login, :index, :logout, :destroy, :send_contact_email]

  def create
    @person = Person.new(create_params)
    a = Person.find_by_email(params[:email])
    
    if a && @person.email == a.email
      render json: {message: 'this email already exist'}
    else 
      @person.save
        if @person.save
            render json: @person
            UserMailer.simple_email(@person).deliver_now
        else
            render json: {message:"not valid couldn't save it"}
        end
    end
  end

  def show
    person = Person.find params[:id]
    render json: person , include:[:pets, :reputations] 
  end

  def index #solo como admin
      people = Person.all
      render json: people , include:[:pets]
  end

  def login
    @personL = Person.find_by_email(params[:email])
    if @personL && @personL.authenticate(params[:password])
      @personL.token = SecureRandom.uuid.gsub(/\-/,'')     
      @personL.save
      render json: @personL
    else
      render json: {message: 'UserNotFound'}, status: :not_found
    end  
    #@personL = Person.find_by_email(params[:email])
    #return  @personL
  end

  def logout
     person = Person.find(params[:id])
     person.token = nil
     person.save
     render json: person
  end

  def destroy #solo como admin
    person = Person.find(params[:id])
    person.delete_related_pets
    person.destroy
    if person.delete
      render json: {menssage: "has been destroyed"}
    else render json: {menssage: "still here"}
    end
  end

  def update 
    b = Person.find(params[:id])
    b.update(allowed_params)
    render json: b
  end

  def send_contact_email #el front agarra el user_id del gato
   @person = Person.find(params[:id]) # y llega aqui 
   @getter = Person.new(params.permit(:email))
  
     UserMailer.notification_email(@getter.email, @person).deliver_now
     render json: @getter
  end

  protected

  def allowed_params
    params.permit(:phone_number)
  end

  def r_not_found(error)
    render json: {error: error.message}, status: :r_not_found
  end

  def create_params
    params.permit(:username, :password, :email, :phone_number)  
  end
  
  def params_log_in
    params.permit(:email, :password)
  end

end
