class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  rescue_from ActiveRecord::RecordNotFound, :with => :r_not_found

  protected

  def r_not_found(error)
		render json: {error: error.message}, status: :r_not_found
	end

  def authenticate
  	authenticate_or_request_with_http_token do |token|
      @current_person = Person.find_by(token: token)
    end    
  end
end
