class ReputationsController < ApplicationController
	rescue_from ActiveRecord::RecordNotFound, :with => :r_not_found

	def create
		reputation = Reputation.new(create_params)
    reputation.save
    render json: reputation
  end

  def index 
    reputations = Reputation.all
    render json: reputations , include:[:person]
  end

  

  protected

  def r_not_found(error)
    render json: {error: error.message}, status: :r_not_found
  end

  def create_params
    params.permit(:person_id, :vote, :review)	
  end

end
