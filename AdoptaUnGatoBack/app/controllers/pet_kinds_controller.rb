class PetKindsController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, :with => :r_not_found

  def create
    pet_kind = PetKind.new(create_params)
    pet_kind.save
    if pet_kind.save
      render json: pet_kind
      else
        render json: {menssage: "invalid species"}
    end
    
  end

  def show
    pet_kind = PetKind.find params[:id]
    render json: pet_kind , include:[:pets]
  end

  def index 
      pet_kinds = PetKind.all
      render json: pet_kinds , include:[:pets]
  end

   def destroy #solo como admin
    specie = PetKind.find(params[:id])
    specie.delete_related_pets
    specie.destroy
    if specie.delete
      render json: {menssage: "has been destroyed"}
    else render json: {menssage: "still here"}
    end
  end


  
  

  protected

  def r_not_found(error)
    render json: {error: error.message}, status: :r_not_found
  end

  def create_params
    params.permit(:species) 
  end

end
