class PetsController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, :with => :r_not_found
  
  def create
    pet = Pet.new(create_params)
    pet.save

    if pet.save
        render json: pet
    else
      render json: {message: "invalid pet"}
    end
    
  end

  def show
    pet = Pet.find params[:id]
    render json: pet , include:[:pet_kind, :person]
  end

  def index 
      pets = Pet.all
      render json: pets
  end

  
  def destroy #despues que ya lo adopten este metodo lo hace el que da en adopcion
    pet = Pet.find(params[:id]).destroy
    if pet.delete
      render json: {message: "has been destroyed"}
    else render json: {message: "still here"}
    end
  end

  def update #cuando le den adoptar este metodo lo hace el adoptador
    pet = Pet.find(params[:id])
    pet.update(allowed_params_update)
    render json: pet
  end

  protected

  def allowed_params_update
    params.permit(:adopted)  
  end

  def r_not_found(error)
    render json: {error: error.message}, status: :r_not_found
  end

  def create_params
    params.permit(:pet_name, :picture, :sex, :ubication, :video, :color, :age, :description, :person_id, :pet_kind_id)  
  end

end
