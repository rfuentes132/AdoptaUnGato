class ApplicationMailer < ActionMailer::Base
  default from: "AdoptaUnGato"
  layout 'mailer'
end