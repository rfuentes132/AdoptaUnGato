class UserMailer < ApplicationMailer

  def simple_email(user)
    @user = user
    mail(to: @user.email, subject:  "Hola #{@user.username}", body: {:person => @user}) do |format|
    format.html {render 'user_mailer'}
    end
  end

  def notification_email(mailed_to, person)
    @mailed_to = mailed_to
    @person = person
    mail(to: @mailed_to, subject:  "#{@person.username} esta interesado en tus mascotas", body: {:person => @person}) do |format|
    format.html {render 'notification_mailer'}
    end
  end

  def attached_email(file_name, email)  
    mail(to: email, subject: "new mail" ) do |format|
    format.html {render 'user_mailer'}
    attachments["new_meme"] = File.read("img/" + file_name)
    end
  end
end
