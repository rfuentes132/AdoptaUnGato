(function() {
  'use strict';

  angular
    .module('adoptaUnGatoFront')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
