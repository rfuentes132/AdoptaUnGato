(function() {
  'use strict';

  angular
    .module('adoptaUnGatoFront', ['ngAnimate', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'toastr']);

})();
