/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('adoptaUnGatoFront')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
