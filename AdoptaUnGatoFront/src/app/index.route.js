(function() {
  'use strict';

  angular
    .module('adoptaUnGatoFront')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/home/home.html',
        controller: 'HomeController',
        controllerAs: 'home'
      })
      .state('header', {
        views: {
          'header@': {
            templateUrl: 'app/header/header.html',
            controller: 'HeaderController',
            controllerAs: 'header'
          }
        }
      });


    $urlRouterProvider.otherwise('/');
  }

})();
